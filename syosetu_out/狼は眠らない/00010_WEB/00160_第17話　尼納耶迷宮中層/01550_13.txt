艾達陷入了沉思。

是在思考〈迷路之龍〉和〈尖岩〉的事吧。
以這狀態繼續迷宮探索很危險。

雷肯雖然也想過，就放棄前進，直接回到地上，但這樣會養成逃跑的習慣。當發生困難事態，或是心中有所牽掛時，就會向後方退步。要是這樣的話，當置身於無法逃脫的狀況時，只有破滅一途。這裡有必要讓她能向前進，一步也行。

「艾達。」
「嗯？」
「別在意。」
「嗚，嗚嗯。」
「不過說了也沒用吧。」
「很可憐不是嗎。〈迷路之龍〉跟〈尖岩〉。」
「是阿。」

會同情別人的心，是艾達的優點，而這優點救了雷肯的命。
因此，沒打算否定那優點。

「如果帶上〈迷路之龍〉在這階層前進的話，妳覺得會怎麼樣？」
「誒？雷肯的話，就會輕輕鬆鬆地踏破不是嗎？」
「在這場合，妳覺得〈尖岩〉會怎麼做？」
「誒？嗯嗯，會怎樣呢。不知道。」
「會跟上來吧。」
「誒。為什麼？」
「我們〈虹石〉，在這迷宮沒有任何實績。」
「嗯。」
「但是，〈尖岩〉有著輝煌的實績以及名聲。」
「嗯。」
「那個〈尖岩〉將自己承擔的委託，交給來歷不明的新來的傢伙們，自己則擺出與我無關的臉。這種事做得了嗎？」
「做，做不到，吧。」
「〈尖岩〉如果不表示自己的地位在〈虹石〉之上，就保不住面子。這樣的〈尖岩〉會跟在後頭。他們會做什麼？」
「誰，誰知道呢。」
「可能會做什麼，也可能不會做什麼。也就是說，我們的前方會有魔獸，而後方會有一群不知道會做什麼的實力者。這個狀況危不危險？」
「危險，也說不定。」
「〈迷路之龍〉也是，雖然看起來很冷靜，但才剛失去兩個同伴。應該很動搖，也很不安吧。這種狀態的人，不知道會做些什麼。〈尖岩〉的女魔法使也動搖得很厲害。」
「這麼說來也是呢。」
「背負著兩支這樣的隊伍，是沒辦法好好探索的。然後，如果我們失敗的話，〈迷路之龍〉就有全滅的危險，〈尖岩〉也不會安然無恙。」
「嗯。」
「也就是說，接受這委託的話，三支隊伍中的不少人，會有受傷或死亡的可能性。但是，不接受委託的話，就一個人都不會死。」
「這樣阿。是這樣呢。」
「艾達。」
「嗯。」
「做冒険者這一行，就會看到可憐的人。但要是因此讓心情搖擺不定，就沒辦法和魔獸戰鬥。在這種狀態戰鬥，就會死。」
「嗚，嗚嗯。」
「同情他人的心很珍貴。但是，探索的時候，要把它忘掉。不忘掉，自己就會死。」
「嗯。知道了。」

（雖然說明得有點強硬）
（但多少有讓艾達的思考轉向別處了吧）

雷肯沒辦法，好好地表達自己的想法。

總而言之，迷宮是個嚴厲的世界。這件事，不讓艾達刻骨銘心地感受到可不行。
所以，才沒有接受卡加爾的委託。不能接受。
因為自己一行人有實力，有餘裕，所以就算背負多少算是負擔的隊伍，要踏破這階層也很容易，抱著這種想法的人，是沒辦法在迷宮存活下去的。
就算是最大限度地減輕了負擔，能盡情發揮力量的狀態，也還是有喪失性命的危險，這就是迷宮。

此外，在迷宮裡，摸不清性情的人的請求和提案，最好盡可能別去接受。雷肯嘗了好幾次苦頭，才學到了這件事。
將來，當艾達置身於相似的狀況時，能回想起雷肯今天的態度，然後拒絶對方的提案的話，這樣就好。

事實上，拒絶讓〈尖岩〉同行，只帶上〈迷路之龍〉的話，應該不會有多少危險。
但要是接受了這條件，艾達就會誤以為在這種場合幫助對方也沒關係吧。

雖然也是因為不想暴露本領，但為了不讓艾達在這點有所誤會，就絶對不能接受卡加爾的委託。

「好。阿利歐斯。赫蕾絲。久等了。去戰鬥吧。」
「能稍等一下嗎。」
「怎麼了。」
「來決定陣形吧。」
「陣形？」
「沒錯。站的位置，攻擊順序。使用的招式也好好決定會比較好。」
「不需要。」
「不會不需要。就算在騎士團，陣形也是連攜的基本中的基本。」
「騎士團的常識，在這裡就先忘掉。迷宮的連攜，不是那種東西。」
「不。王国騎士團現在也是會去芬凱爾迷宮的阿。」
「赫蕾絲小姐。」
「阿利歐斯殿。就讓我說吧。」
「不行。」
「什麼？」
「您從何時起能對雷肯先生命令了呢？」
「這不是命令。是在提案。」
「決定陣形和戰法，確實是集團戰的基本。」
「對吧。不會有錯的。」
「但是，那是不論對上什麼對手都能通用的標準行動方式，不論讓哪一隊的哪個位置的哪位騎士加入，都能有所機能，所編出的泛用連攜法。通用性很高就是了。另外還有一點，騎士團使用的陣形，是以集團對集團的戰鬥來想定的。」
「這哪裡有問題了。」
「誰知道呢？我不知道。但是，艾達小姐和我，是為了跟雷肯先生學習在此之上的方法，才來到這裡的。如果對此有意見的話，請現在就離開隊伍。」

總是很溫和的阿利歐斯所說出的，出乎意料的嚴厲說詞，讓赫蕾絲一瞬擺出了不滿的表情。但是，大口吸氣並吐出後，便這麼說道。

「雷肯殿。是我失禮了。會遵從您的指令的。」

在進行著這些對話時，一行人也在前進，但雷肯此時停了下來。

「這塊岩石的對面有魔獸。我從正面衝進去。阿利歐斯從右邊，赫蕾絲從左邊。艾達最初就從沙場外射箭來吸引敵人的注意。之後就是回復役。走了。」
「是。」
「等一。」

雷肯照著自己所說的突擊了。阿利歐斯馬上跟在後頭。赫蕾絲晚了一步。艾達還沒擺好姿勢，雷肯就衝出去了，雖然慌張地舉起弓，但已經錯過了該射出箭的時機。
雷肯以盾牌防住溶解液，然後直接在頭部給予一擊。趁著魔獸的動作在一瞬間停下的破綻，阿利歐斯將左前腳從根部砍下。

受到了雷肯接著向頭部打出的一擊，魔獸死了。
雷肯將魔獸剩下的七隻腳也砍下，從關節切斷，然後收進〈收納〉。
兩隻眼也挖了下來收進〈收納〉。
將腹部切裂，把毒袋放進採取用的專用容器後，也收進了〈收納〉。

「不，所以說，雷肯殿。那個〈箱〉到底是怎麼回事。說到底，〈箱〉是在哪裡？」
「就說別問了。」

有件讓人開心的事。

從魔獸身上採取的魔石，大了許多。
將這稱為大型魔石也不為過吧。
接下來，就是大型魔石的寶庫。

戰鬥持續了好幾次。從第二次開始，艾達似乎也做好了心理準備，在雷肯接敵前，會將箭矢射進上腹部，吸引敵人的注意。
因為會突然衝出去，所以總會是近似於亂戰的狀態。赫蕾絲被敵人攻擊了好幾次，受了傷。雷肯也被溶解液濺到好幾次，臉跟喉嚨被灼傷。

這些都被艾達的〈回復〉治得漂漂亮亮的。

「雷肯殿。雖然我不會對指示有意見，但能麻煩別將那大劍揮到旁邊嗎。危險得不像話。」
「閃開就好。」
「太亂來了。」

也不吃午餐，戰鬥一直持續到了下午，最後打倒了兩隻大型個體。
得到的素材量有大型個體兩隻，普通個體十隻。寶箱沒有出現。

「好。這次的探索就到此結束。從明天開始的三天是休養日。讓身體充分休息吧。赫蕾絲。素材要在明天賣掉。陪我去吧。辛苦各位了。沖個澡休息之後，就到〈傑德的店〉集合。跟之前一樣，晚飯我請客。盡情吃喜歡的食物吧。」