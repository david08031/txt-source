# 含有日文的章節段落

[TOC]

## 00000_序章/00000_1話　闇に呑まれた闇勇者

- 俺のことを好きでいてくれる人が、三人もいるんだ。嬉しくないわけがないんだよな。
- 「すっ、すごいわ」
- 「私もだ。もっと修行して、役に立ってみせる」
- 我慢してもやっぱりニヤついてしまう。意識しないようにしていけれど、今日は俺の誕生日なのだ。去年のように、盛大に祝ってくれるのだろうか。
- 「まま待って！？　あんた、もしかして西恩樣じゃないですか！？」


## 00000_序章/00005_2話　死にたがりの白狐

- 忘れもしない８歳の誕生日。
- 運命というものがあるのであれば、俺はきっと凶星のもとに生まれたのだろう。
- 誰かを愛しては先立たれ、誰かを信じては裏切られてきた。強くなって世界を救えば、何かが變わると信じて必死だった。
- 「ううう゛ぅ⋯⋯あぁぁ⋯⋯」
- ボサボサの白毛に赤毛が少し混じるのが目印だ。最初にこれに遭遇するとは、さすが暗黑の森と言うべきか。
- こんな相手ではダメだ。


## 00000_序章/00020_4話　色褪せない勇者の心

- 「それは復興に使ってください。というか俺、光物は苦手なので。⋯⋯元闇勇者だけにね」
- 「罪なんかじゃない！そうじゃないのよ！あんたには何の責任もない。いつも村のために、お姉醬のために動いてた。誰かのために立派に勇者やってた！」
- 呀咧ていただろうか？　世界に對する俺の力は無力で、本當はいつも自信がなかった。
- ウインクをする哈庫に、俺は滿面の笑顏を返す。
- ほんの一週間前までは──


## 00010_1章/00030_5話　鉄壁の都市とキュオオオン

- 『へえー，這個以前好像在那兒聽過』
- 『────キォォオオオオオオオオオン！』


## 00010_1章/00040_6話　冒険者登録

- 『こん！こんこん！』
- 『但是我，要是放鬆的話就會「わっふぅうう！」那樣的了！要是不這個樣的話就麻煩了。』
- 『わっふあうううう！』


## 00010_1章/00050_7話　弱鬼ごっこ

- 『對於阿克斯大人，我也很尊敬的哦。他很帥對吧（チラッチラッ）』
- 『這樣啊。我也很想見見他呢。也許意外地會近期到訪這城市來喲！（チラッチラッ）』
- 『呋呋ふん，我這邊收集得比較多也說不定呢～一、二、三⋯⋯⋯⋯有二十二個呀！』


## 00010_1章/00070_9話　魔力オーバー

- 「ウッ！？　⋯⋯おえええええ」
- 『うひー、這肉又嫩又好吃啊』


## 00010_1章/00080_10話　貴重な薬草採取

- 「嗯？　ンィイアアアア！」
- 『きゃ、きゃっ、西恩！這裡的水很舒服啊』
- バシャバシャ，靈巧地搖動著腳和尾巴要弄走水分的哈庫的模樣，讓裝著大人的西恩也被觸動了童心。
- 「へっくし！」
- 『ふぁくしょん！』


## 00010_1章/00090_11話　色.......彩狐

- 「うおおっ」
- 「熱ちっ！？」
- 「きゃぁっ⋯⋯！」
- 「ケルゥゥ！」
- 「がが、あが⋯⋯」


## 00010_1章/00100_12話　ギルドマスター、驚く

- 在公會內確認數目後，錢包的錢增加到３５萬ガウン


## 00010_1章/00110_13話　生活ギルドに入ろう！

- 『もぐもぐ今日もぐ會去もぐもぐ公會もぐ嗎もぐ？』
- 『あああああああああああああ──ッ』


## 00010_1章/00120_14話　奇術のスリ師

- 「ほおおおお！？」
- 「へー，有能把那個魔石搞掂的傢伙在啊？還真有奇特的傢伙存在呢」
- 「へー，就憑那傢伙就想抓住我嗎？」


## 00010_1章/00130_15話　闇分身トラップ

- スリ師那傢伙在眼前消失了。
- 『能解除透明化嗎？看不見的話可能會咬錯位置。バキボシャガギュ⋯⋯這樣的感覺』


## 00010_1章/00140_16話　黒と黒の闘い　前編

- 「へっ，那位先生呢。用名字來稱呼吧。總不會是像我那狗屎般的名字一樣的吧。」
- 「真是徒勞的努力，辛苦了啊。くくく」
- 將肉體變質為黑暗，在黑暗中自由移動的技能稱為『黑泳』。『黑泳こくえい』


## 00010_1章/00145_17話　黒と黒の闘い　後編

- 在那裡馬上發動了最擅長的暗魔法『黑手こくしゅ』
- 「がああああああああああああああああああ」
- 「⋯⋯あぁあうううぅぁ，可惡，你這傢伙，才是本體嗎」
- コツ、コツ西恩的腳步聲在深夜裡迴響著。


## 00010_1章/00150_18話　オークの森　前編

- ということで西恩はその料理店に向かうが、晝なのに閉まっていた。看板に本日は休みと書いてあるので間違いない。
- 『すみません、エバドンさんいらっしゃいます？』
- 「ああ、俺達で良ければ探してきます。調理して欲しいものがあるので」
- 『可能ならバレないように追跡したいものですが』
- 『可能ですよ～。ブタさんのニオイがプンプンですから』


## 00010_1章/00155_19話　オークの森　後編

- 「『ごくり⋯⋯っ』」
- 「ふぅぅぅ，這邊也好吃到爆啊！」
- 『はふはふっ、ごきゅんごきゅん──』


## 00010_1章/00170_21話　神眼の勇者

- 「うぐぐぐ⋯⋯」


## 00010_1章/00180_22話　新しい旅へ

- ブン　ブン　ブンブンブンブンブンブン
- 『わっふうぅうううううううううう──！！』
- ダッダッダッダッダ、體格健壯的白狐氣勢洶洶地穿過大地
